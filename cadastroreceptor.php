<?php
    session_start();
    include "ferramentas/inicia.php";
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/estilo.css">
        <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <title>The Power Of One</title>
    </head>
    <body>
    <nav class="navbar navbar-default">
        <div class="container-fluid bd-1">
            <a class="navbar-brand bd-nav" href="Index.php"><img src="imagens/logoNav.png"></a><br>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="fn-1"><a href="sobre.php">Sobre Nós</a></li>
                        <li class="fn-1"><a href="cestas.php">Cestas</a></li>
                        <li class="fn-1"><a href="fale.php">Fale Conosco</a></li>
                        <li class="fn-1"><a href="voluntario.php">Seja um Voluntário</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="fn-1"><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
            </div>
        </div>
    </nav>
        <div class="container formulario cadastrorecep">
                <h1 id="tituloCadastro">Cadastre-se</h1>

                <form class="container campos" method="post" action="RCadastroreceptor.php">
                    <?php 
                        if($_SESSION["msg_sistema"] == "E-mail já cadastrado!"){            
                            echo '<div class="alert alert-danger"> E-mail já cadastrado!';    
                        }
                        else{
                        
                        }       
                    ?>
                    <div class="input-group pd-1">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="nomereceptor" type="text" class="form-control" name="nomereceptor" placeholder="Digite o nome" required autofocus>
                    </div><br>

                    <div class="input-group pd-1">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input id="email" type="email" class="form-control" name="emailreceptor" placeholder="Digite o email" required>
                    </div><br>

                    <div class="input-group pd-1">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="password" type="password" class="form-control" name="senhareceptor" placeholder="Digite a senha" required>
                    </div><br>

                    <div class="form-check form-check-inline">
                        *Você está desempregado?<br>
                        <input class="form-check-input" type="radio" name="desemprego" id="" value="sim" required>
                        Sim 
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="desemprego" id="" value="nao">  
                        Não<br><br>
                    </div>

                    <div class="form-check form-check-inline">
                        *Escolha seu sexo:<br>
                        <input class="form-check-input" type="radio" name="genero" id="" value="M" required>
                        Masculino
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="genero" id="" value="F">  
                        Feminino<br><br>
                    </div>

                    <div class="form-check form-check-inline">
                        *Você é aposentado?<br>
                        <input class="form-check-input" type="radio" name="aposentado" id="" value="sim" required>
                        Sim 
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="aposentado" id="" value="nao">  
                        Não<br><br>
                    </div>

                    *Digite a quantidade de pessoas <input type="number" name="qntFamilia" min="0" max="17" required><br><br>

                    *Quantos sal&aacute;rios  m&iacute;nimos sua fam&iacute;lia arrecada: <input type="number" name="qntSal" required><br><br>

                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-log-in"></i> Enviar</button>
                    </div><br>

                </form>
        </div><br><br><br>

        <!--<div class="container formulario socio">

                    <div class="input-group pd-1">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="namea" type="text" class="form-control" name="nome" placeholder="Nome Completo" required autofocus>
                    </div><br>

                    <div class="input-group pd-1">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="emailreceptor" type="email" class="form-control" name="emailrec" placeholder="Digite o email" required>
                    </div><br>

                    <div class="form-check form-check-inline">
                        Você está desempregado?<br>
                        <input class="form-check-input" type="radio" name="resposta" id="" value="sim">
                        Sim 
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="resposta" id="" value="nao">  
                        Não<br><br>
                    </div>

                    <div class="form-check form-check-inline">
                        Escolha seu sexo:<br>
                        <input class="form-check-input" type="radio" name="genero" id="" value="M">
                        Masculino
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="genero" id="" value="F">  
                        Feminino<br><br>
                    </div>

                    <div class="form-check form-check-inline">
                        Você é aposentado?<br>
                        <input class="form-check-input" type="radio" name="situacao" id="" value="sim">
                        Sim 
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="situacao" id="" value="nao">  
                        Não<br><br>
                    </div>

                    Digite a quantidade de pessoas <input type="number" name="quantidadefamilia" min="0" max="17"><br><br>

                    Quantos sal&aacute;rios  m&iacute;nimos sua fam&iacute;lia arrecada: <input type="text"><br>

                    <div class="input-group pd-1">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="" type="radio" class="form-control" name="" value="Sim">

                    </div>--><br>
        </div>

    </body>
</html>     