<?php
    session_start();
    include "ferramentas/inicia.php";
    verify_login();
    if ($_SESSION["nome"] != ""){

?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/estilo.css">
        <link href="https://fonts.googleapis.com/css?family=Old+Standard+TT&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <title>The Power Of One</title>
    </head>
    <body>     

    <nav class="navbar navbar-default">
            <div class="container-fluid bd-1">
                <?php
                    if ($_SESSION["nome"] != ""){
                        if($_SESSION["tipo"] == 1){
                            echo '<a class="navbar-brand bd-nav" href="Indexdoador.php"><img src="imagens/logoNav.png"></a><br>';
                        }
                        else{
                            echo '<a class="navbar-brand bd-nav" href="Indexreceptor.php"><img src="imagens/logoNav.png"></a><br>';
                        }    
                    }
                    else
                    {
                        echo '<a class="navbar-brand bd-nav" href="Index.php"><img src="imagens/logoNav.png"></a><br>';
                    }
                ?>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="fn-1"><a href="sobre.php">Sobre Nós</a></li>
                        <li class="fn-1"><a href="doar.php">Doar</a></li>
                        <li class="fn-1"><a href="fale.php">Fale Conosco</a></li>
                        <li class="fn-1"><a href="voluntario.php">Seja um Voluntário</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                            if ($_SESSION["nome"] != ""){
                                echo '<li class="fn-1"><a href="sair.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>';
                            }
                            else{
                                echo '<li class="fn-1"><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>';
                            }
                        ?>
                    </ul>
                </div>
            </div>
    </nav>
        <div class="jumbotron" style="height:50px;">

            <div class="container foto doacao" style="height:70px;margin-top:-45px;">
                <h1 style="font-size:37px;height:100px;height:40px;margin-top:10px;">Faça sua Doação</h1>
            </div>

        </div>
        
        <div class="container doarform">
            <form class="container campos" method="post" action="RDoar.php" style="width:1155px;">

                <div class="form-check form-check-inline">
                        <br>
                        <label class="containervalordoado" style="font-size:30px;float:left;margin-right:30px;">R$50,00
                            <input class="form-check-input" type="radio" name="valordoacao" id="" value="50" >
                            <span class="checkmark"></span>
                        </label>
                        <label class="containervalordoado" style="font-size:30px;float:left;margin-right:30px;">R$100,00
                            <input class="form-check-input" type="radio" name="valordoacao" id="" value="100" >
                            <span class="checkmark"></span>
                        </label>
                        <label class="containervalordoado" style="font-size:30px;float:left;margin-right:30px;">R$150,00
                            <input class="form-check-input" type="radio" name="valordoacao" id="" value="150" >
                            <span class="checkmark"></span>
                        </label>
                        <label class="containervalordoado" style="font-size:30px;float:left;margin-right:30px;">R$200,00
                            <input class="form-check-input" type="radio" name="valordoacao" id="" value="200" >
                            <span class="checkmark"></span>
                        </label>
                        <label class="containervalordoado" style="font-size:30px;float:left;margin-right:30px;">R$250,00
                            <input class="form-check-input" type="radio" name="valordoacao" id="" value="250" >
                            <span class="checkmark"></span>
                        </label>
                        <label class="containervalordoado" style="font-size:30px;float:left;">R$300,00
                            <input class="form-check-input" type="radio" name="valordoacao" id="" value="300" >
                            <span class="checkmark"></span>
                        </label>
                        <br>
                </div>
                <div class="input-group-btn">
                    <button class="btn btn-default" type="button" style="margin-bottom:5px;margin-right:50px;" onclick="mostrar()"><i class="glyphicon glyphicon-plus"></i> Doar Outro Valor</button>
                    
                    <input id="permitir" type="text" style="width:20%;margin-bottom:5px;"class="form-control" name="outrovalor" placeholder="EX: 1000,00" disabled>

                        <script>
                            function mostrar(){
                                document.getElementById("permitir").disabled= false;
                            }
                        </script>

                    <button class="btn btn-default" type="submit" style="margin-right:10px;"><i class="glyphicon glyphicon-plus"></i> Doar</button>
                    
                </div>
                <script>
                    function myFunction(){
                        document.getElementById("outro").style.display = inline-block;
                    }
                </script>
                <br>
            </form>
        </div>
            <div class="container-fluid footer">
                <footer>
                    <div class="container name">
                        <p>&copy;(The)Power Of One Foundation</p>
                    </div>
                </footer>
            </div>
    </body>
</html>
<?php
}
else{
    header("location: login.php");
}
?>