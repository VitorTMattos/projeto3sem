<?php
      function connection(){
            global $conn;
            $conn = mysqli_connect(DB_SERVER, DB_USER, DB_PSW, DB_DATABASE);
            if(!$conn){
                  die("Falha ao conectar com o servidor: " . mysqli_connect_error());
            }
            else{
                  echo "Conexão efetuada com sucesso!";
            }
      }
      function verify_login(){
            if(!isset($_SESSION["nome"])){
                  header("location: ../Index.php");
                  exit;
            }
      }
?>
