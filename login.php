<?php
session_start();
$err = filter_input(INPUT_GET, 'erro',FILTER_SANITIZE_FULL_SPECIAL_CHARS);
//print_r($_SESSION);
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/estilo.css">
        <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <title>The Power Of One</title>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid bd-1">
                <a class="navbar-brand bd-nav" href="Index.php"><img src="imagens/logoNav.png"></a><br>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="fn-1"><a href="sobre.php">Sobre Nós</a></li>
                        <li class="fn-1"><a href="fale.php">Fale Conosco</a></li>
                        <li class="fn-1"><a href="voluntario.php">Seja um Voluntário</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="fn-1"><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
            </div>
        </div>
    </nav>
        <div class="container formulario">
            <h1 id="tituloLogin">Login</h1>
            <form class="container campos" method="post" action="RLogin.php">
                <div class="input-group pd-1">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input id="email" type="email" class="form-control" name="email" placeholder="Digite o email" required autofocus>
                </div><br>
                <div class="input-group pd-1">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input id="password" type="password" class="form-control" name="senha" placeholder="Digite a senha" required>
                </div><br>    
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-log-in"></i> Entrar</button>
                </div><br>
                <a href="precadastro.php">Cadastre-se aqui!&nbsp;</a>
            </form>
        </div>
    </body>
</html>