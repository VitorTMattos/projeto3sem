<?php
    session_start();
    include "ferramentas/inicia.php";
    $_SESSION["msg_sistema"] = "";
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/estilo.css">
        <link href="https://fonts.googleapis.com/css?family=Old+Standard+TT&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <title>The Power Of One</title>
    </head>
    <body>     

    <nav class="navbar navbar-default">
            <div class="container-fluid bd-1">
                <a class="navbar-brand bd-nav" href="Index.php"><img src="imagens/logoNav.png"></a><br>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="fn-1"><a href="sobre.php">Sobre Nós</a></li>
                        <li class="fn-1"><a href="doar.php">Doações</a></li>
                        <li class="fn-1"><a href="fale.php">Fale Conosco</a></li>
                        <li class="fn-1"><a href="voluntario.php">Seja um Voluntário</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="fn-1"><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>
                </div>
            </div>
    </nav>
        <div class="jumbotron" style="height:50px;">
            <div class="container foto doacao" style="height:70px;margin-top:-45px;">
                <h1 style="font-size:37px;height:100px;height:40px;margin-top:10px;">Doações Recebidas</h1>
            </div>
        </div>

        <!--<div class="container-fluid produtos">-->
                <div class="container-fluid explicacao doar">
                    <div class="container-fluid recebidas">
                        <div class="container-fluid>">
                            <p style="font-size:50px;">O total de doações recebidas é de R$1350,00</p>
                        </div>
                    </div>
                </div>
        <div class="container-fluid buttons"></div>
        </div>

            <div class="container-fluid footer">
                <footer>
                    <div class="container name">
                        <p>&copy;(The)Power Of One Foundation</p>
                    </div>
                </footer>
            </div>
    </body>
</html>