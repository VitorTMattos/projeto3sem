<?php
    session_start();
    include "ferramentas/inicia.php";
    verify_login();
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/estilo.css">
        <link href="https://fonts.googleapis.com/css?family=Old+Standard+TT&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <title>The Power Of One</title>
    </head>
    <body>     
        <nav class="navbar navbar-default">
            <div class="container-fluid bd-1">
                <?php
                    if ($_SESSION["nome"] != ""){
                        if($_SESSION["tipo"] == 1){
                            echo '<a class="navbar-brand bd-nav" href="Indexdoador.php"><img src="imagens/logoNav.png"></a><br>';
                        }
                        else{
                            echo '<a class="navbar-brand bd-nav" href="Indexreceptor.php"><img src="imagens/logoNav.png"></a><br>';
                        }    
                    }
                    else
                    {
                        echo '<a class="navbar-brand bd-nav" href="Index.php"><img src="imagens/logoNav.png"></a><br>';
                    }
                ?>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="fn-1"><a href="sobre.php">Sobre Nós</a></li>
                        <!--<li class="fn-1"><a href="cestas.php">Cestas</a></li>-->
                        <?php
                            if ($_SESSION["nome"] != ""){
                                if($_SESSION["tipo"] == 1){
                                    echo '<li class="fn-1"><a href="doar.php">Doar</a></li>';
                                }   
                            }
                        ?>
                        <li class="fn-1"><a href="fale.php">Fale Conosco</a></li>
                        <li class="fn-1"><a href="voluntario.php">Seja um Voluntário</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                            if ($_SESSION["nome"] != ""){
                                echo '<li class="fn-1"><a href="sair.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>';
                            }
                            else{
                                echo '<li class="fn-1"><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>';
                            }
                        ?>
                    </ul>
            </div>
        </div>
    </nav>
        <div class="container formulario voluntario">
            <h1 id="tituloVoluntario">Seja um Voluntário</h1>
            <form class="container campos" method="post" action="RVoluntario.php">
                <div class="input-group pd-1">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input id="namea" type="text" class="form-control" name="nome" placeholder="Digite o nome" required autofocus>
                </div><br>
                <div class="input-group pd-1">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input id="email" type="email" class="form-control" name="email" placeholder="Digite o email" required>
                </div><br>
                <textarea name="motivo" class="description" placeholder="Digite aqui suas razões para se voluntariar"></textarea><br>
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-send"></i> Enviar</button>
                </div><br>
            </form>
        </div>

        <div class="container-fluid footer">
                <footer>
                    <div class="container name">
                        <p>&copy;(The)Power Of One Foundation</p>
                    </div>
                </footer>
        </div>

    </body>
</html>